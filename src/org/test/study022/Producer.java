package org.test.study022;

import java.util.Random;

public class Producer implements Runnable {

	private Broker broker;

	public Producer(Broker broker) {
		this.broker = broker;
	}

	@Override
	public void run() {
		Random rand = new Random();
		while (true) {
			try {
				broker.putTaskToQueue(new PrimeNumbers(rand.nextInt(10000)));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
