package org.test.study022;

import java.util.LinkedHashSet;
import java.util.concurrent.BlockingQueue;

public interface Broker {

	public BlockingQueue<LinkedHashSet<Integer>> getTasksAnswers();

	public void putTasksAnswer(LinkedHashSet<Integer> taskAnswers)
			throws InterruptedException;

	public BlockingQueue<PrimeNumbers> getTaskQueue();

	public void putTaskToQueue(PrimeNumbers task) throws InterruptedException;

}
