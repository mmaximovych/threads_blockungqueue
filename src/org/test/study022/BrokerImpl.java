package org.test.study022;

import java.util.LinkedHashSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BrokerImpl implements Broker {

	private volatile BlockingQueue<LinkedHashSet<Integer>> tasksAnswers;
	private volatile BlockingQueue<PrimeNumbers> taskQueue;

	public BrokerImpl(int taskQueueCapacity, int tasksAnswersCapacity) {
		tasksAnswers = new LinkedBlockingQueue<LinkedHashSet<Integer>>(
				tasksAnswersCapacity);
		taskQueue = new LinkedBlockingQueue<PrimeNumbers>(taskQueueCapacity);
	}

	public BlockingQueue<LinkedHashSet<Integer>> getTasksAnswers() {
		return tasksAnswers;
	}

	public void putTasksAnswer(LinkedHashSet<Integer> taskAnswers)
			throws InterruptedException {
		this.tasksAnswers.put(taskAnswers);
	}

	public BlockingQueue<PrimeNumbers> getTaskQueue() {
		return taskQueue;
	}

	public void putTaskToQueue(PrimeNumbers task) throws InterruptedException {
		this.taskQueue.put(task);
	}

}
