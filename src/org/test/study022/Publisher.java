package org.test.study022;

public class Publisher implements Runnable {
	private Broker broker;

	public Publisher(Broker broker) {
		this.broker = broker;
	}

	private String answer() throws InterruptedException {
		String answer = "Answer is " + broker.getTasksAnswers().take().size()
				+ " prime numbers;  " + Thread.currentThread().getName();
		return answer;
	}

	@Override
	public void run() {
		while (true) {
			try {
				System.out.println(answer());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
