package org.test.study022;



import java.util.LinkedHashSet;

public class PrimeNumbers {
private int numbers;

	public PrimeNumbers(int numbers){
		this.numbers = numbers;
	}

	public LinkedHashSet<Integer> prime() {
		int check;
		LinkedHashSet<Integer> primeNumbers = new LinkedHashSet<Integer>();
		primeNumbers.add(2);
		primeNumbers.add(3);
		primeNumbers.add(5);
		primeNumbers.add(7);
		label: for (int i = 10; i <= numbers; i++) {
			for (int numDigit = 2; numDigit <= 9; numDigit++) {
				if ((i % numDigit) == 0) {
					continue label;
				}
			}
			check = (int) Math.sqrt(i);
			for (int pn : primeNumbers) {
				if ((i % pn) == 0)
					continue label;
				if (pn >= check) {
					primeNumbers.add(i);
					continue label;
				}
			}
		}

		return primeNumbers;
	}
}

