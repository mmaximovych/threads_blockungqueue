package org.test.study022;

public class Worker implements Runnable {

	private Broker broker;

	public Worker(Broker broker) {
		this.broker = broker;
	}

	private void putAnswerToPublisher() throws InterruptedException {
		broker.putTasksAnswer(broker.getTaskQueue().take().prime());
	}

	@Override
	public void run() {
		while (true) {
			try {
				putAnswerToPublisher();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
