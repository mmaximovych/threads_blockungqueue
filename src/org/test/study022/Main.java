package org.test.study022;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		ExecutorService es1 = Executors.newFixedThreadPool(10);
		ExecutorService es2 = Executors.newFixedThreadPool(10);
		ExecutorService es3 = Executors.newFixedThreadPool(10);
		Broker br = new BrokerImpl(10, 10);
		Publisher pb = new Publisher(br);
		Worker wr = new Worker(br);
		Producer pr = new Producer(br);
		for (int i = 0; i < 10; i++) {
			// es1.submit(pb);
			es1.execute(pb);
			// es2.submit(wr);
			es2.execute(wr);
			// es3.submit(pr);
			es3.execute(pr);
		}
	}
}
